To run
======

Install:

    apt-get install im-config ibus ibus-gtk ibus-gtk3 ibus-qt4 libqt5gui5 python3-speechd

Start ibus:

    ibus-daemon &

Start ibus-braille:

    ./ibus-braille.py &

Enable ibus support:

    export GTK_IM_MODULE=ibus
    export QT_IM_MODULE=ibus
    export XMODIFIERS=@im=ibus

    gedit &

Start natbraille (get sources from Vivien or Samuel, they are not public yet):

    ./startServer

Typing
======

To type Braille, press control-alt-shift together (ibus-braille says the current
type mode). The keyboard homerow now behaves like a Braille typewriter.

To revert to typing normally, press left control-alt-shift together again
(ibus-braille says "Braille disabled").

By default, litteral Braille is used. To switch to grade 2 BFU table
contracted Braille, type left control-shift together (ibus-braille says
'contracted mode'). To switch back to litteral Braille, type left control-shift
together again (ibus-braille says 'literal mode')


to type maths, press right control-shift (ibus-braille says "maths mode"). No
uncontraction is done, and Braille shows up as Braille patterns (except A to Z,
a to z and 0 to 9).

To convert between braille dots and StarMath, type control-m. For instance,
type

⠠⠄⠦⠁⠖⠩⠴

which shows up as

⠠⠄⠦a⠖3⠴

and conversion with control-m gives

{ ( a + 3 ) }

(with MathML it would give
<math> <mrow xmlns=""> <mo>(</mo> <mi>a</mi> <mo>+</mo> <mn>3</mn> <mo>)</mo> </mrow> </math>
)

and converting back with control-m gives

⠠⠄⠦a⠖3⠴

again.
